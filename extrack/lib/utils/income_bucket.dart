import 'package:extrack/configs/constants.dart';
import 'package:extrack/models/transaction.dart';

class IncomeBucket {
  const IncomeBucket({
    required this.category,
    required this.incomes,
  });

  IncomeBucket.forCategory(List<Transaction> allIncomes, this.category)
      : incomes =
            allIncomes.where((income) => income.category == category).toList();

  final Category category;
  final List<Transaction> incomes;

  double get totalIncomes {
    double sum = 0;

    for (final income in incomes) {
      sum += income.amount; // sum = sum + income.amount
    }

    return sum;
  }
}
