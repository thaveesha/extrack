import 'package:extrack/widgets/global_widgets/bottom_navbar_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:extrack/widgets/chart/income_chart_widget.dart';
import 'package:extrack/widgets/income/incomes_list_widget.dart';
import 'package:extrack/models/transaction.dart';
import 'package:extrack/providers/transactions.dart';

class IncomesScreen extends StatefulWidget {
  const IncomesScreen({super.key});

  @override
  State<IncomesScreen> createState() {
    return _IncomesScreenState();
  }
}

class _IncomesScreenState extends State<IncomesScreen> {
  void _removeIncome(Transaction income) {
    final transactions =
        Provider.of<Transactions>(context, listen: false).getTransactionList;
    final incomeIndex = transactions.indexOf(income);
    Provider.of<Transactions>(context, listen: false)
        .removeTransaction(incomeIndex);
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: const Duration(seconds: 5),
        content: const Text('Income deleted.'),
        action: SnackBarAction(
          label: 'Undo',
          onPressed: () {
            Provider.of<Transactions>(context, listen: false)
                .insertTransaction(incomeIndex, income);
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final transactions =
        Provider.of<Transactions>(context, listen: false).getTransactionList;
    Widget mainContent = Center(
      child: Column(
        children: [
          const SizedBox(height: 32),
          Image.asset(
            'assets/images/waiting.png',
            height: 100,
          ),
          const SizedBox(height: 32),
          Text(
            'No incomes found. Start adding some!',
            style: Theme.of(context)
                .textTheme
                .bodyMedium!
                .copyWith(fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );

    if (transactions.isNotEmpty) {
      mainContent = IncomesListWidget(
        onRemoveIncome: _removeIncome,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'ExTrack [INCOME]',
          style: Theme.of(context).textTheme.titleLarge,
        ),
        centerTitle: true,
      ),
      body: Column(
        children: [
          IncomeChartWidget(context: context),
          Expanded(
            child: mainContent,
          ),
        ],
      ),
      bottomNavigationBar: const BottomNavbarWidget(),
    );
  }
}
