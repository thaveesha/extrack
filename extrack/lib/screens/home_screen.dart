import 'package:extrack/models/transaction.dart';
import 'package:extrack/widgets/chart/income_chart_widget.dart';
import 'package:extrack/widgets/global_widgets/bottom_navbar_widget.dart';
import 'package:extrack/widgets/home/home_screen_list_widget.dart';
import 'package:extrack/widgets/home/summery_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:extrack/providers/transactions.dart';
import 'package:extrack/widgets/home/home_screen_list_item_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  void _removeTransaction(Transaction transaction) {
    final transactions =
        Provider.of<Transactions>(context, listen: false).getTransactionList;
    final transactionIndex = transactions.indexOf(transaction);
    Provider.of<Transactions>(context, listen: false)
        .removeTransaction(transactionIndex);
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: const Duration(seconds: 5),
        content: const Text('Income deleted.'),
        action: SnackBarAction(
          label: 'Undo',
          onPressed: () {
            Provider.of<Transactions>(context, listen: false)
                .insertTransaction(transactionIndex, transaction);
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final transactions =
        Provider.of<Transactions>(context, listen: false).getTransactionList;
    Widget mainContent = Center(
      child: Column(
        children: [
          const SizedBox(height: 32),
          Image.asset(
            'assets/images/waiting.png',
            height: 100,
          ),
          const SizedBox(height: 32),
          Text(
            'No incomes found. Start adding some!',
            style: Theme.of(context)
                .textTheme
                .bodyMedium!
                .copyWith(fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );

    if (transactions.isNotEmpty) {
      mainContent = HomeScreenListWidget(
        onRemoveTransaction: _removeTransaction,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'ExTrack',
        ),
        centerTitle: true,
      ),
      body: Column(
        children: [
          const SummeryWidget(),
          Expanded(
            child: mainContent,
          ),
        ],
      ),
      bottomNavigationBar: const BottomNavbarWidget(),
    );
  }
}
