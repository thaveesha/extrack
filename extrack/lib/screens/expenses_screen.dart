import 'package:extrack/widgets/global_widgets/bottom_navbar_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:extrack/models/transaction.dart';
import 'package:extrack/widgets/chart/expense_chart_widget.dart';
import 'package:extrack/widgets/expense/expenses_list_widget.dart';
import 'package:extrack/providers/transactions.dart';

class ExpensesScreen extends StatefulWidget {
  const ExpensesScreen({super.key});

  @override
  State<ExpensesScreen> createState() {
    return _ExpensesScreenState();
  }
}

class _ExpensesScreenState extends State<ExpensesScreen> {
  void _removeExpense(Transaction expense) {
    final transactions =
        Provider.of<Transactions>(context, listen: false).getTransactionList;
    final expenseIndex = transactions.indexOf(expense);
    Provider.of<Transactions>(context, listen: false)
        .removeTransaction(expenseIndex);
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: const Duration(seconds: 5),
        content: const Text('Expense deleted.'),
        action: SnackBarAction(
          label: 'Undo',
          onPressed: () {
            Provider.of<Transactions>(context, listen: false)
                .insertTransaction(expenseIndex, expense);
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final transactions =
        Provider.of<Transactions>(context, listen: false).getTransactionList;
    Widget mainContent = Center(
      child: Column(
        children: [
          const SizedBox(height: 32),
          Image.asset(
            'assets/images/waiting.png',
            height: 100,
          ),
          const SizedBox(height: 32),
          Text(
            'No expenses found. Start adding some!',
            style: Theme.of(context)
                .textTheme
                .bodyMedium!
                .copyWith(fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );

    if (transactions.isNotEmpty) {
      mainContent = ExpensesListWidget(
        onRemoveExpense: _removeExpense,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'ExTrack [EXPENSE]',
          style: Theme.of(context).textTheme.titleLarge,
        ),
        centerTitle: true,
      ),
      body: Column(
        children: [
          ExpenseChartWidget(context: context),
          Expanded(
            child: mainContent,
          ),
        ],
      ),
      bottomNavigationBar: const BottomNavbarWidget(),
    );
  }
}
