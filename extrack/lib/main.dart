import 'package:extrack/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:extrack/providers/transactions.dart';
import 'package:extrack/widgets/global_widgets/bottom_navbar_widget.dart';
import 'package:extrack/configs/custom_colors.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => Transactions()),
        ChangeNotifierProvider(create: (context) => SelectedIndexProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData().copyWith(
          appBarTheme: const AppBarTheme().copyWith(
            backgroundColor: CustomColors.blue,
          ),
          bottomAppBarTheme: BottomAppBarTheme(
            color: CustomColors.blue,
          ),
          cardTheme: const CardTheme().copyWith(
            elevation: 1,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            color: Colors.white,
            margin: const EdgeInsets.symmetric(
              horizontal: 16,
              vertical: 8,
            ),
          ),
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
              foregroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              backgroundColor: CustomColors.blue,
              textStyle: const TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: 'RobotoMono',
              ),
            ),
          ),
          textButtonTheme: TextButtonThemeData(
            style: TextButton.styleFrom(
              foregroundColor: CustomColors.blue,
              textStyle: const TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: 'RobotoMono',
              ),
            ),
          ),
          textTheme: ThemeData().textTheme.copyWith(
                titleLarge: const TextStyle(
                  fontFamily: 'RobotoMono',
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 20,
                ),
                titleMedium: TextStyle(
                  fontFamily: 'RobotoMono',
                  fontWeight: FontWeight.bold,
                  color: CustomColors.blue,
                  fontSize: 20,
                ),
                bodyLarge: const TextStyle(
                  fontFamily: 'RobotoMono',
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 16,
                ),
                bodyMedium: const TextStyle(
                  fontFamily: 'RobotoMono',
                  fontWeight: FontWeight.normal,
                  color: Colors.black,
                  fontSize: 14,
                ),
              ),
        ),
        home: const HomeScreen(),
      ),
    );
  }
}
