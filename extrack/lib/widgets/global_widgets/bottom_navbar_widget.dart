import 'package:extrack/configs/custom_colors.dart';
import 'package:flutter/material.dart';

import 'package:extrack/screens/expenses_screen.dart';
import 'package:extrack/screens/home_screen.dart';
import 'package:extrack/screens/incomes_screen.dart';
import 'package:extrack/screens/new_transaction_screen.dart';
import 'package:provider/provider.dart';

class SelectedIndexProvider with ChangeNotifier {
  int _selectedIndex = 0;

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int newIndex) {
    _selectedIndex = newIndex;
    notifyListeners();
  }
}

class BottomNavbarWidget extends StatefulWidget {
  const BottomNavbarWidget({Key? key}) : super(key: key);

  @override
  State<BottomNavbarWidget> createState() => _BottomNavbarWidgetState();
}

class _BottomNavbarWidgetState extends State<BottomNavbarWidget> {
  @override
  Widget build(BuildContext context) {
    int item = 0;
    final selectedIndexProvider = Provider.of<SelectedIndexProvider>(context);
    final selectedItem = selectedIndexProvider.selectedIndex;

    return BottomAppBar(
      padding: const EdgeInsets.all(8),
      shape: const CircularNotchedRectangle(),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                setState(() {
                  item = 0;
                });
                selectedIndexProvider.selectedIndex = 0;
                Future.delayed(
                  const Duration(milliseconds: 50),
                  () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const HomeScreen(),
                      ),
                    );
                  },
                );
              },
              child: Container(
                padding: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  color: selectedItem == 0 ? Colors.white : CustomColors.blue,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Icon(
                  Icons.home_outlined,
                  color: selectedItem == 0 ? CustomColors.blue : Colors.white,
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  item = 1;
                });
                selectedIndexProvider.selectedIndex = 1;
                Future.delayed(Duration(milliseconds: 50), () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const IncomesScreen(),
                    ),
                  );
                });
              },
              child: Container(
                padding: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  color: selectedItem == 1 ? Colors.white : CustomColors.blue,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Icon(
                  Icons.arrow_circle_up,
                  color: selectedItem == 1 ? CustomColors.blue : Colors.white,
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  item = 2;
                });
                selectedIndexProvider.selectedIndex = 2;
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ExpensesScreen(),
                  ),
                );
              },
              child: Container(
                padding: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  color: selectedItem == 2 ? Colors.white : CustomColors.blue,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Icon(
                  Icons.arrow_circle_down,
                  color: selectedItem == 2 ? CustomColors.blue : Colors.white,
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  item = 3;
                });
                selectedIndexProvider.selectedIndex = 3;
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const NewTransactionScreen(),
                  ),
                );
              },
              child: Container(
                padding: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  color: selectedItem == 3 ? Colors.white : CustomColors.blue,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Icon(
                  Icons.add,
                  color: selectedItem == 3 ? CustomColors.blue : Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
