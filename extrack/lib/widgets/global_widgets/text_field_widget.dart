import 'package:flutter/material.dart';

class TextFieldWidget extends StatelessWidget {
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final Widget? suffixWidget;
  final Icon? suffixIcon;
  final int? maxLines;
  final int? maxLength;
  final TextCapitalization textCapitalization;
  final void Function()? onTap;
  final bool readOnly;
  final String hintText;

  const TextFieldWidget({
    Key? key,
    this.controller,
    this.keyboardType,
    this.suffixWidget,
    this.suffixIcon,
    this.maxLines,
    this.maxLength,
    this.textCapitalization = TextCapitalization.none,
    this.onTap,
    this.readOnly = false,
    this.hintText = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      readOnly: readOnly,
      controller: controller,
      maxLines: maxLines,
      maxLength: maxLength,
      textCapitalization: textCapitalization,
      onTap: onTap,
      keyboardType: keyboardType,
      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
            fontWeight: FontWeight.normal,
          ),
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: const TextStyle(
          fontSize: 16,
          fontFamily: 'RobotoMono',
          fontWeight: FontWeight.normal,
          color: Colors.grey,
        ),
        suffix: suffixWidget,
        suffixIcon: suffixIcon,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: const BorderSide(width: 1, color: Colors.grey),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: const BorderSide(width: 1, color: Colors.grey),
        ),
      ),
    );
  }
}
