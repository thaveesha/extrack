import 'package:extrack/configs/custom_colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:extrack/configs/constants.dart';
import 'package:extrack/widgets/global_widgets/chart_bar_widget.dart';
import 'package:extrack/providers/transactions.dart';
import 'package:extrack/utils/expense_bucket.dart';

class ExpenseChartWidget extends StatelessWidget {
  final BuildContext context;

  const ExpenseChartWidget({super.key, required this.context});

  List<ExpenseBucket> get buckets {
    final expenseTransactions =
        Provider.of<Transactions>(context, listen: false)
            .getTransactionList
            .where((transaction) => transaction.type == Type.Expense)
            .toList();
    return [
      ExpenseBucket.forCategory(expenseTransactions, Category.Food),
      ExpenseBucket.forCategory(expenseTransactions, Category.Entertainment),
      ExpenseBucket.forCategory(expenseTransactions, Category.Travel),
      ExpenseBucket.forCategory(expenseTransactions, Category.Work),
      ExpenseBucket.forCategory(expenseTransactions, Category.Health),
      ExpenseBucket.forCategory(expenseTransactions, Category.Other),
    ];
  }

  double get maxTotalExpense {
    double maxTotalExpense = 0;

    for (final bucket in buckets) {
      if (bucket.totalExpenses > maxTotalExpense) {
        maxTotalExpense = bucket.totalExpenses;
      }
    }

    return maxTotalExpense;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 16,
          horizontal: 8,
        ),
        width: double.infinity,
        height: MediaQuery.of(context).size.height * 1 / 4,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(
            colors: [
              CustomColors.blue.withOpacity(0.35),
              CustomColors.blue.withOpacity(0.0)
            ],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
          ),
        ),
        child: Column(
          children: [
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  for (final bucket in buckets) // alternative to map()
                    ChartBar(
                      fill: bucket.totalExpenses == 0
                          ? 0
                          : bucket.totalExpenses / maxTotalExpense,
                    ),
                ],
              ),
            ),
            const SizedBox(height: 12),
            Row(
              children: buckets // for ... in
                  .map(
                    (bucket) => Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 4),
                        child: Icon(
                          Constants.categoryIcons[bucket.category],
                          color: CustomColors.blue.withOpacity(0.7),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            )
          ],
        ),
      ),
    );
  }
}
