import 'package:extrack/configs/custom_colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:extrack/configs/constants.dart';
import 'package:extrack/widgets/global_widgets/chart_bar_widget.dart';
import 'package:extrack/providers/transactions.dart';
import 'package:extrack/utils/income_bucket.dart';

class IncomeChartWidget extends StatelessWidget {
  final BuildContext context;

  const IncomeChartWidget({super.key, required this.context});

  List<IncomeBucket> get buckets {
    final incomeTransactions = Provider.of<Transactions>(context, listen: false)
        .getTransactionList
        .where((transaction) => transaction.type == Type.Income)
        .toList();
    return [
      IncomeBucket.forCategory(incomeTransactions, Category.Food),
      IncomeBucket.forCategory(incomeTransactions, Category.Entertainment),
      IncomeBucket.forCategory(incomeTransactions, Category.Travel),
      IncomeBucket.forCategory(incomeTransactions, Category.Work),
      IncomeBucket.forCategory(incomeTransactions, Category.Health),
      IncomeBucket.forCategory(incomeTransactions, Category.Other),
    ];
  }

  double get maxTotalIncome {
    double maxTotalIncome = 0;

    for (final bucket in buckets) {
      if (bucket.totalIncomes > maxTotalIncome) {
        maxTotalIncome = bucket.totalIncomes;
      }
    }

    return maxTotalIncome;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 16,
          horizontal: 8,
        ),
        width: double.infinity,
        height: MediaQuery.of(context).size.height * 1 / 4,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(
            colors: [
              CustomColors.blue.withOpacity(0.35),
              CustomColors.blue.withOpacity(0.0)
            ],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
          ),
        ),
        child: Column(
          children: [
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  for (final bucket in buckets) // alternative to map()
                    ChartBar(
                      fill: bucket.totalIncomes == 0
                          ? 0
                          : bucket.totalIncomes / maxTotalIncome,
                    )
                ],
              ),
            ),
            const SizedBox(height: 12),
            Row(
              children: buckets // for ... in
                  .map(
                    (bucket) => Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 4),
                        child: Icon(
                          Constants.categoryIcons[bucket.category],
                          color: CustomColors.blue.withOpacity(0.7),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            )
          ],
        ),
      ),
    );
  }
}
