import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:extrack/configs/constants.dart';
import 'package:extrack/models/transaction.dart';
import 'package:extrack/providers/transactions.dart';

class HomeScreenListItemWidget extends StatelessWidget {
  final int index;
  const HomeScreenListItemWidget({required this.index, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<Transactions>(builder: (context, transactions, child) {
      final expense = transactions.getTransactionList[index];
      final extendedNotes = expense.notes.split('\n');
      return Card(
        color: Colors.white,
        child: transactions.getTransactionList[index].notes == ''
            ? listItemWidget(context, expense)
            : InkWell(
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      title: Text('Notes',
                          style: Theme.of(context).textTheme.titleMedium),
                      content: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: extendedNotes
                              .map(
                                (line) => Text(
                                  line,
                                ),
                              )
                              .toList(),
                        ),
                      ),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Text('Okay'))
                      ],
                    ),
                  );
                },
                child: listItemWidget(context, expense),
              ),
      );
    });
  }

  Widget listItemWidget(BuildContext context, Transaction expense) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.primary.withOpacity(0.3),
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 16,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            expense.title,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          const SizedBox(height: 8),
          Row(
            children: [
              Text(
                '${expense.amount.toStringAsFixed(2)} LKR',
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              const Spacer(),
              Row(
                children: [
                  Icon(Constants.typeIcons[expense.type]),
                  Icon(Constants.categoryIcons[expense.category]),
                  const SizedBox(width: 16),
                  Text(expense.formattedDate),
                  const SizedBox(width: 16),
                  Icon(
                      expense.notes != ''
                          ? Icons.description
                          : Icons.description_outlined,
                      size: 14,
                      color: Colors.black),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
