import 'package:extrack/configs/custom_colors.dart';
import 'package:extrack/widgets/home/home_screen_list_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:extrack/models/transaction.dart';
import 'package:extrack/widgets/expense/expense_item_widget.dart';
import 'package:extrack/providers/transactions.dart';

class HomeScreenListWidget extends StatelessWidget {
  const HomeScreenListWidget({
    super.key,
    required this.onRemoveTransaction,
  });

  final void Function(Transaction expense) onRemoveTransaction;

  @override
  Widget build(BuildContext context) {
    return Consumer<Transactions>(
      builder: (context, transactions, child) {
        return ListView.builder(
          itemCount: transactions.getTransactionList.length,
          physics: const BouncingScrollPhysics(),
          itemBuilder: (context, index) {
            final reversedIndex =
                transactions.getTransactionList.length - 1 - index;
            final transaction = transactions.getTransactionList[reversedIndex];
            return Dismissible(
              key: ValueKey(transaction),
              background: Container(
                color: CustomColors.blue.withOpacity(0.75),
                margin: EdgeInsets.symmetric(
                  horizontal: Theme.of(context).cardTheme.margin!.horizontal,
                ),
              ),
              onDismissed: (direction) {
                onRemoveTransaction(transaction);
              },
              child: HomeScreenListItemWidget(index: reversedIndex),
            );
          },
        );
      },
    );
  }
}
