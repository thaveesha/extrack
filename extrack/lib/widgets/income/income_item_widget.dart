import 'package:extrack/configs/custom_colors.dart';
import 'package:flutter/material.dart';

import 'package:extrack/configs/constants.dart';
import 'package:extrack/models/transaction.dart';
import 'package:provider/provider.dart';
import 'package:extrack/providers/transactions.dart';

class IncomeItemWidget extends StatelessWidget {
  const IncomeItemWidget(this.index, {super.key});

  final int index;

  @override
  Widget build(BuildContext context) {
    return Consumer<Transactions>(builder: (context, transactions, child) {
      final filteredTransactions =
          transactions.getFilteredTransactionList(Type.Income);
      if (index >= 0 && index < filteredTransactions.length) {
        final income = filteredTransactions[index];
        final extendedNotes = income.notes.split('\n');
        return Card(
          color: Colors.white,
          child: transactions.getTransactionList[index].notes == ''
              ? listItemWidget(context, income)
              : InkWell(
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        title: Text('Notes',
                            style: Theme.of(context).textTheme.titleMedium),
                        content: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: extendedNotes
                                .map(
                                  (line) => Text(
                                    line,
                                  ),
                                )
                                .toList(),
                          ),
                        ),
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: const Text('Okay'))
                        ],
                      ),
                    );
                  },
                  child: listItemWidget(context, income),
                ),
        );
      }
      // Handle the case when the index is out of range
      return const SizedBox.shrink();
    });
  }

  Container listItemWidget(BuildContext context, Transaction income) {
    return Container(
      decoration: BoxDecoration(
        color: CustomColors.blue.withOpacity(0.3),
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 16,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            income.title,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          const SizedBox(height: 8),
          Row(
            children: [
              Text(
                '${income.amount.toStringAsFixed(2)} LKR',
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              const Spacer(),
              Row(
                children: [
                  Icon(Constants.categoryIcons[income.category]),
                  const SizedBox(width: 16),
                  Text(income.formattedDate),
                  const SizedBox(width: 16),
                  Icon(
                      income.notes != ''
                          ? Icons.description
                          : Icons.description_outlined,
                      size: 14,
                      color: Colors.black),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
