import 'package:extrack/configs/custom_colors.dart';
import 'package:flutter/material.dart';

import 'package:extrack/widgets/income/income_item_widget.dart';
import 'package:extrack/models/transaction.dart';
import 'package:provider/provider.dart';
import 'package:extrack/providers/transactions.dart';

class IncomesListWidget extends StatelessWidget {
  const IncomesListWidget({
    super.key,
    required this.onRemoveIncome,
  });

  final void Function(Transaction income) onRemoveIncome;

  @override
  Widget build(BuildContext context) {
    return Consumer<Transactions>(builder: (context, transactions, child) {
      return ListView.builder(
        itemCount: transactions.getTransactionList.length,
        physics: const BouncingScrollPhysics(),
        itemBuilder: (contex, index) {
          final reversedIndex =
              transactions.getTransactionList.length - 1 - index;
          final transaction = transactions.getTransactionList[reversedIndex];
          return Dismissible(
            key: ValueKey(transaction),
            background: Container(
              color: CustomColors.blue.withOpacity(0.75),
            ),
            onDismissed: (direction) {
              onRemoveIncome(transaction);
            },
            child: IncomeItemWidget(reversedIndex),
          );
        },
      );
    });
  }
}
