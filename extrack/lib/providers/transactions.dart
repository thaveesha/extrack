import 'package:flutter/material.dart';

import 'package:extrack/configs/constants.dart';
import 'package:extrack/models/transaction.dart';

class Transactions extends ChangeNotifier {
  final List<Transaction> _transactions = [
    Transaction(
      title: 'Test 1',
      notes: '',
      amount: 10.99,
      date: DateTime.now(),
      category: Category.Work,
      type: Type.Expense,
    ),
    Transaction(
      title: 'Test 2',
      notes: '',
      amount: 11.99,
      date: DateTime.now(),
      category: Category.Work,
      type: Type.Income,
    ),
    Transaction(
      title: 'Test 3',
      notes: '',
      amount: 13.99,
      date: DateTime.now(),
      category: Category.Work,
      type: Type.Expense,
    ),
    Transaction(
      title: 'Test 4',
      notes: '',
      amount: 14.99,
      date: DateTime.now(),
      category: Category.Work,
      type: Type.Income,
    ),
  ];

  void addTransaction(Transaction transaction) {
    _transactions.add(transaction);
    notifyListeners();
  }

  void removeTransaction(int index) {
    if (index >= 0 && index < _transactions.length) {
      _transactions.removeAt(index);
      notifyListeners();
    }
  }

  void insertTransaction(int index, Transaction transaction) {
    if (index >= 0 && index <= _transactions.length) {
      _transactions.insert(index, transaction);
      notifyListeners();
    }
  }

  List<Transaction> get getTransactionList {
    return _transactions;
  }

  List<Transaction> getFilteredTransactionList(Type? type) {
    final List<Transaction> list = [];
    for (int i = 0; i < _transactions.length; i++) {
      if (_transactions[i].type == type) {
        list.add(_transactions[i]);
      }
    }
    return list;
  }
}
