import 'package:uuid/uuid.dart';
import 'package:intl/intl.dart';

import 'package:extrack/configs/constants.dart';

final formatter = DateFormat.yMd();

const uuid = Uuid();

class Transaction {
  Transaction({
    required this.title,
    required this.notes,
    required this.amount,
    required this.date,
    required this.category,
    required this.type,
  }) : id = uuid.v4();

  final String id;
  final String title;
  final String notes;
  final double amount;
  final DateTime date;
  final Category category;
  final Type type;

  String get formattedDate {
    return formatter.format(date);
  }
}
